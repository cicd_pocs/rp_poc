# rp_poc

```bash
git clone https://gitlab.com/cicd_pocs/rp_poc
```

```bash
delirium@rpi3b:~/rp_poc $ docker-compose up -d
Creating network "rp_poc_default" with the default driver
Creating nginx_new   ... done
Creating nginx_old   ... done
Creating nginx_proxy ... done
delirium@rpi3b:~/rp_poc $ curl localhost:80
<html>
    <head>
        <title>
            This is new site
        </title>
    </head>
    <body>
        <h1>✅✅✅ BITRIX CMS ✅✅✅</h1>
        <p>This is the new site</p>
    </body>
</html>
delirium@rpi3b:~/rp_poc $ curl localhost:80/clients/
<html>
    <head>
        <title>
            This is the old site
        </title>
    </head>
    <body>
        <h1>⛔️⛔️⛔️ MODX CMS - Clients ⛔️⛔️⛔️</h1>
        <p>This is the old site</p>
    </body>
</html>
```
